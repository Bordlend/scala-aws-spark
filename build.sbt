name := "scala-aws-spark"
version := "0.1"
scalaVersion := "2.13.6"

lazy val kinesisProducer = (project in file("kinesis-producer"))
  .settings(
    name := "kinesis-producer",
    scalaVersion := "2.13.6",

    libraryDependencies ++= Seq(
      "com.amazonaws" % "aws-java-sdk" % "1.12.2"
    )
  )

lazy val sparkJob = (project in file("spark-job"))
  .settings(
    name := "spark-job",
    scalaVersion := "2.13.6",
  )

lazy val root = (project in file("."))
  .aggregate(kinesisProducer)
  .aggregate(sparkJob)
