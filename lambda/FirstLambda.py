import json
import boto3
import base64

s3 = boto3.client('s3')


def lambda_handler(event, context):
    bucket = 'scala-aws-spark'
    file_name = 'transJson' + '.json'
    for record in event['Records']:
        data = base64.b64decode(record['kinesis']['data'])

        transaction_to_upload = {'transactionId': str(data), 'type': 'PURCHASE', 'amount': 20, 'customerId': 'CID-11111'}

        upload_byte_stream = bytes(json.dumps(transaction_to_upload).encode('UTF-8'))

        s3.put_object(Bucket=bucket, Key=file_name, Body=upload_byte_stream)
