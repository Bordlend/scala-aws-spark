import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.{ClientConfiguration, Protocol}
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder
import com.amazonaws.services.kinesis.model.{PutRecordsRequest, PutRecordsRequestEntry}

import java.nio.ByteBuffer
import java.util
import java.util.Properties
import scala.io.Source
import scala.util.Random

object Main extends App {

  val properties = getProperties

  val kinesisClient = AmazonKinesisClientBuilder.standard
    .withCredentials(new ProfileCredentialsProvider(properties.getProperty("aws.profile.name")))
    .withClientConfiguration(getClientConfiguration)
    .withRegion(properties.getProperty("aws.region"))
    .build

  val data = Random.between(1, 100)
  println(data)

  val putRecordsRequest = new PutRecordsRequest()
    .withStreamName(properties.getProperty("aws.stream.name"))
    .withRecords(convertToJavaList(List(0, 1).map(i => new PutRecordsRequestEntry()
      .withData(ByteBuffer.wrap(String.valueOf(data).getBytes))
      .withPartitionKey(String.format("partitionKey-%d", i)))))

  val putRecordsResult = kinesisClient.putRecords(putRecordsRequest)

  println(s"Put Result: $putRecordsResult")

  def getClientConfiguration: ClientConfiguration = {
    new ClientConfiguration()
      .withProtocol(Protocol.HTTPS)
  }

  def convertToJavaList(list: List[PutRecordsRequestEntry]): util.List[PutRecordsRequestEntry] = {
    import scala.jdk.CollectionConverters._
    list.asJava
  }

  def getProperties: Properties = {
    val url = getClass.getResource("application.properties")
    val properties = new Properties()

    if (url != null)
      properties.load(Source.fromURL(url).bufferedReader())
    properties
  }

}

//Code for in code credentials
//val awsCredentials = new BasicAWSCredentials("", "")
//clientBuilder.setCredentials(new AWSStaticCredentialsProvider(awsCredentials))
